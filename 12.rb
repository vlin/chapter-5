# implement a GooglePhoto which can show a photo and play a youtube movie

class MediaPlayer
  def play(media)
    media.play
  end
end

class GooglePhoto
  def initialize(medias)
    @medias = medias
  end

  def play
    @medias.each do |media|
      MediaPlayer.new.play(media)
    end
  end
end

class Photo
  def play
    puts "show a photo"
  end
end

class YoutubeMovie
  def play
    puts "play a youtube movie"
  end
end


# use it
a_google_photo = [Photo.new, YoutubeMovie.new]
google_photo = GooglePhoto.new(a_google_photo)
MediaPlayer.new.play(google_photo)


# output
# show a photo
# play a youtube movie


