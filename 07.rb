class Parser
  def parse(type)
    puts 'The Parser class received the parse method'

    if type == :xml
      puts 'An instance of the XmlParser class received the parse message'
    elsif type == :json
      puts 'An instance of the JsonParser class received the parse message'
    end
  end
end

# use it
Parser.new.parse(:xml)
Parser.new.parse(:json)

# output
# An instance of the XmlParser class received the parse message
# An instance of the JsonParser class received the parse message





# refactoring to duck typing
# achieve run time polymorphism

class Parser
  def parse(parser)
    parser.parse
  end
end

class XmlParser
  def parse
    puts 'An instance of the XmlParser class received the parse message'
  end
end

class JsonParser
  def parse
    puts 'An instance of the JsonParser class received the parse message'
  end
end

# use it
Parser.new.parse(XmlParser.new)
Parser.new.parse(JsonParser.new)

# output
# An instance of the XmlParser class received the parse message
# An instance of the JsonParser class received the parse message


