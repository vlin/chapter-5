class MediaPlayer
  def play(media)
    media.play
  end
end

class Dvd
  def play
    puts "play a movie"
  end
end

class Cd
  def play
    puts "play a music"
  end
end

class Album
  def play
    puts "play a slide"
  end
end

# use it
MediaPlayer.new.play(Dvd.new)
MediaPlayer.new.play(Cd.new)
MediaPlayer.new.play(Album.new)

# output
# play a movie
# play a music
# play a slide


