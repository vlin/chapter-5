# extend MediaPlayer to play a pdf document

class MediaPlayer
  def play(media)
    media.play
  end
end

class Pdf
  def play
    puts "show a pdf document"
  end
end

# use it
MediaPlayer.new.play(Pdf.new)

# output
# show a pdf document

