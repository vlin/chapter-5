module AdminHelper
  def book_state_label(book)
    case book.aasm_state
      when 'created'
        "<span class='label label-info'>#{book.aasm_state}</span>".html_safe
      when 'parsing'
        "<span class='label label-warning'>#{book.aasm_state}</span>".html_safe
      when 'parsing_success'
        "<span class='label label-success'>#{book.aasm_state}</span>".html_safe
      when 'parsing_unsuccess'
        "<span class='label label-danger'>#{book.aasm_state}</span>".html_safe
      when 'published'
        "<span class='label label-inverse'>#{book.aasm_state}</span>".html_safe
      else
        "<span class='label label-danger'>#{book.aasm_state}</span>".html_safe
    end
  end

  def word_state_label(word)
    case word.aasm_state
      when 'created'
        "<span class='label label-info'>#{word.aasm_state}</span>".html_safe
      when 'grabbing'
        "<span class='label label-warning'>#{word.aasm_state}</span>".html_safe
      when 'grabbing_success'
        "<span class='label label-success'>#{word.aasm_state}</span>".html_safe
      when 'grabbing_unsuccess'
        "<span class='label label-danger'>#{word.aasm_state}</span>".html_safe
      when 'grabbing_finish'
        "<span class='label label-primary'>#{word.aasm_state}</span>".html_safe
      else
        "<span class='label label-danger'>#{word.aasm_state}</span>".html_safe
    end
  end

  def role_label(role)
    case role.name
      when 'admin'
        "<span class='label label-danger'>#{role.name}</span>".html_safe
      when 'paid_user'
        "<span class='label label-warning'>#{role.name}</span>".html_safe
      else
        "<span class='label label-danger'>#{role.name}</span>".html_safe
    end
  end

  def tag_label(tag)
    "<span class='label label-info'>#{tag.name}</span>".html_safe
  end

  def word_label(word)
    "<span class='label label-inverse'>#{word.name}</span>".html_safe
  end

  def unconfirmed_user_badge(user)
    if user.confirmed_at.nil?
      "<span class='badge badge-danger'>n</span>".html_safe
    end
  end

  def show_button(link)
    "<a href='#{link}' class='btn btn-xs btn-success' target='_blank'><i class='ace-icon fa fa-check'></i></a>".html_safe
  end

  def edit_button(link, open_new_window)
    if open_new_window == true
      "<a href='#{link}' class='btn btn-xs btn-info' target='_blank'><i class='ace-icon fa fa-pencil'></i></a>".html_safe
    else
      "<a href='#{link}' class='btn btn-xs btn-info'><i class='ace-icon fa fa-pencil'></i></a>".html_safe
    end
  end

  def destroy_button(link, enabled)
    if enabled == true
      "<a href='#{link}' data-confirm='Are you sure?' rel='nofollow' data-method='delete' class='btn btn-xs btn-danger' id='destroy'><i class='ace-icon fa fa-trash-o'></i></a>".html_safe
    else
      "<a href='#{link}' data-confirm='Are you sure?' rel='nofollow' data-method='delete' class='btn btn-xs btn-danger' disabled><i class='ace-icon fa fa-trash-o'></i></a>".html_safe
    end
  end

  def parse_word_button(book)
    if !book.content_text.nil? and book.aasm_state == 'created'
      "<a href='#{admin_book_parse_word_path(book)}' class='btn btn-xs btn-warning' id='parse_word'>parse</a>".html_safe
    else
      "<a href='#{admin_book_parse_word_path(book)}' class='btn btn-xs btn-warning' disabled>parse</a>".html_safe
    end
  end

  def publish_book_button(book)
    if book.aasm_state == 'parsing_success'
      "<a href='#{admin_book_publish_path(book)}' data-confirm='Are you sure?' class='btn btn-xs btn-danger' id='publish_book'>publish</a>".html_safe
    else
      "<a href='#{admin_book_publish_path(book)}' data-confirm='Are you sure?' class='btn btn-xs btn-danger' disabled>publish</a>".html_safe
    end
  end
end