class MediaPlayer
  def play(type)
    case type
    when :dvd
      puts "play a movie"
    when :cd
      puts "play a music"
    when :album
      puts "play a slide"
    end
  end
end

# use it
MediaPlayer.new.play(:dvd)
MediaPlayer.new.play(:cd)
MediaPlayer.new.play(:album)

# output
# play a movie
# play a music
# play a slide

