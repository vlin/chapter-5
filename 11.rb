# extend MediaPlayer to play a photo and a youtube movie

class MediaPlayer
  def play(media)
    media.play
  end
end

class Photo
  def play
    puts "show a photo"
  end
end

class YoutubeMovie
  def play
    puts "play a youtube movie"
  end
end


# use it
MediaPlayer.new.play(Photo.new)
MediaPlayer.new.play(YoutubeMovie.new)

# output
# show a photo
# play a youtube movie


